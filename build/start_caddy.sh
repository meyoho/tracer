#!/bin/sh
sed -i "s|PORT|${HTTP_PORT:=80}|g" /stub/caddyfile.json
/stub/caddy run --config /stub/caddyfile.json
