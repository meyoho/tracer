/*

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1beta1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	TraceJobKind = "TraceJob"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// TraceJobSpec defines the desired state of TraceJob
type TraceJobSpec struct {
	// +optional
	Schedule string `json:"schedule,omitempty"`

	// +optional
	BuiltInProbes []string `json:"builtInProbes,omitempty"`

	// +optional
	Probes []Probe `json:"probes,omitempty"`

	// +optional
	SuccessfulJobsHistoryLimit *int32 `json:"successfulJobsHistoryLimit,omitempty"`
	// +optional
	FailedJobsHistoryLimit *int32 `json:"failedJobsHistoryLimit,omitempty"`
}

// TraceJobStatus defines the observed state of TraceJob
type TraceJobStatus struct {
	// Information when was the last time the job was successfully scheduled.
	// +optional
	LastScheduleTime *metav1.Time `json:"lastScheduleTime,omitempty"`
	// +optional
	LastExecuteResult Phase `json:"lastExecuteResult,omitempty"`
	// +optional
	LastExecuteReason string `json:"lastExecuteReason,omitempty"`
}

// +kubebuilder:object:root=true
// +kubebuilder:resource:scope=Cluster
// +kubebuilder:subresource:status

// TraceJob is the Schema for the tracejobs API
type TraceJob struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   TraceJobSpec   `json:"spec,omitempty"`
	Status TraceJobStatus `json:"status,omitempty"`
}

// +kubebuilder:object:root=true

// TraceJobList contains a list of TraceJob
type TraceJobList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []TraceJob `json:"items"`
}

func init() {
	SchemeBuilder.Register(&TraceJob{}, &TraceJobList{})
}
