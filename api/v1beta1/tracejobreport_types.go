/*

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1beta1

import (
	"fmt"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// TraceJobReportSpec defines the desired state of TraceJobReport
type TraceJobReportSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file
	AllAgents []AgentInfo `json:"allAgents"`
	Probes    []Probe     `json:"probes"`
}

const (
	TraceJobReportKind = "TraceJobReport"
)

type Phase string

var (
	Running Phase = "Running"
	Failed  Phase = "Failed"
	Success Phase = "Success"
)

type AddressInfo struct {
	// +optional
	IP string `json:"ip"`
	// +optional
	Port *int32 `json:"port,omitempty"`
	// +optional
	URL string `json:"url,omitempty"`
	// +optional
	Domain string `json:"domain,omitempty"`
	// +optional
	PodName string `json:"podName,omitempty"`
	// +optional
	PodNamespace string `json:"podNamespace,omitempty"`
	// +optional
	NodeName string `json:"nodeName,omitempty"`
	// +optional
	NetworkMode string `json:"networkMode,omitempty"`
}

func (a *AddressInfo) String() string {
	var rv string
	if a.IP != "" {
		rv += fmt.Sprintf("ip: %s,", a.IP)
	}
	if a.Port != nil {
		rv += fmt.Sprintf("port: %d,", *a.Port)
	}
	if a.URL != "" {
		rv += fmt.Sprintf("url: %s,", a.URL)
	}
	if a.Domain != "" {
		rv += fmt.Sprintf("domain: %s,", a.Domain)
	}
	return rv
}

type Probe struct {
	Protocol string `json:"protocol"`
	// +optional
	Source      *AddressInfo `json:"source,omitempty"`
	Destination AddressInfo  `json:"destination"`
	// +optional
	Result bool `json:"result"`
	// +optional
	Reason string `json:"reason,omitempty"`
	// +optional
	Args map[string]string `json:"args,omitempty"`
}

var (
	HostNetwork      = "HostNetwork"
	ContainerNetwork = "ContainerNetwork"
)

type AgentInfo struct {
	PodName      string `json:"podName"`
	PodNamespace string `json:"podNamespace"`
	PodIP        string `json:"podIP"`
	NodeName     string `json:"nodeName"`
	NetworkMode  string `json:"networkMode"`
}

// TraceJobReportStatus defines the observed state of TraceJobReport
type TraceJobReportStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
	ObservedGeneration int64 `json:"observedGeneration,omitempty"`
	Phase              Phase `json:"phase"`
	// +optional
	Reason         string      `json:"reason,omitempty"`
	Detail         []Probe     `json:"detail"`
	StartTime      metav1.Time `json:"startTime"`
	ReportedAgents []AgentInfo `json:"reportedAgents"`
	// +optional
	FinishTime *metav1.Time `json:"finishTime,omitempty"`
}

// +kubebuilder:object:root=true
// +kubebuilder:resource:scope=Cluster
// +kubebuilder:subresource:status
// +kubebuilder:printcolumn:name="Phase",type=string,JSONPath=`.status.phase`
// +kubebuilder:printcolumn:name="Reason",type=string,JSONPath=`.status.reason`
// +kubebuilder:printcolumn:name="StartTime",type=string,JSONPath=`.status.startTime`
// +kubebuilder:printcolumn:name="FinishTime",type=string,JSONPath=`.status.finishTime`

// TraceJobReport is the Schema for the tracejobreports API
type TraceJobReport struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   TraceJobReportSpec   `json:"spec,omitempty"`
	Status TraceJobReportStatus `json:"status,omitempty"`
}

// +kubebuilder:object:root=true

// TraceJobReportList contains a list of TraceJobReport
type TraceJobReportList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []TraceJobReport `json:"items"`
}

func init() {
	SchemeBuilder.Register(&TraceJobReport{}, &TraceJobReportList{})
}

func (rl TraceJobReportList) Len() int {
	return len(rl.Items)
}
func (rl TraceJobReportList) Swap(i, j int) { rl.Items[i], rl.Items[j] = rl.Items[j], rl.Items[i] }
func (rl TraceJobReportList) Less(i, j int) bool {
	return rl.Items[j].CreationTimestamp.Before(&rl.Items[i].CreationTimestamp)
}
