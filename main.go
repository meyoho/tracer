/*

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"flag"
	alaudaiov1beta1 "github.com/alauda/tracer/api/v1beta1"
	"github.com/alauda/tracer/controllers"
	"github.com/alauda/tracer/pkg/config"
	"github.com/alauda/tracer/pkg/utils"
	"github.com/alauda/tracer/pkg/version"
	"k8s.io/apimachinery/pkg/runtime"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	"k8s.io/klog"
	"os"
	ctrl "sigs.k8s.io/controller-runtime"
	// +kubebuilder:scaffold:imports
)

var (
	scheme = runtime.NewScheme()
)

func init() {
	_ = clientgoscheme.AddToScheme(scheme)

	_ = alaudaiov1beta1.AddToScheme(scheme)
	// +kubebuilder:scaffold:scheme
}

func main() {
	klog.InitFlags(nil)
	var metricsAddr string
	var enableLeaderElection bool
	var role string
	flag.StringVar(&role, "role", "", "Role should be manager or agent")
	flag.Parse()
	if role == utils.RoleManager {
		metricsAddr = ":8080"
		enableLeaderElection = true
	} else if role == utils.RoleAgent {
		metricsAddr = ":0"
		enableLeaderElection = false
	} else {
		panic("invalid role")
	}
	config.PrintVar(role)
	version.PrintVersion()

	mgr, err := ctrl.NewManager(ctrl.GetConfigOrDie(), ctrl.Options{
		Scheme:             scheme,
		MetricsBindAddress: metricsAddr,
		LeaderElection:     enableLeaderElection,
		LeaderElectionID:   "tracer-manager-lock",
	})
	if err != nil {
		klog.Error("unable to start manager", err)
		os.Exit(1)
	}

	if err = (&controllers.TraceJobReconciler{
		Client: mgr.GetClient(),
		Role:   role,
		Scheme: mgr.GetScheme(),
	}).SetupWithManager(mgr); err != nil {
		klog.Error("unable to create TraceJob controller", err)
		os.Exit(1)
	}
	if err = (&controllers.TraceJobReportReconciler{
		Client: mgr.GetClient(),
		Role:   role,
		Scheme: mgr.GetScheme(),
	}).SetupWithManager(mgr); err != nil {
		klog.Error("unable to create TraceJobReport controller", err)
		os.Exit(1)
	}
	// +kubebuilder:scaffold:builder

	klog.Infof("starting manager role %s", role)
	if err := mgr.Start(ctrl.SetupSignalHandler()); err != nil {
		klog.Error("problem running manager", err)
		os.Exit(1)
	}
}
