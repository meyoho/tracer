/*

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"fmt"
	tracerv1beta1 "github.com/alauda/tracer/api/v1beta1"
	commonconf "github.com/alauda/tracer/pkg/config/common"
	"github.com/alauda/tracer/pkg/probe"
	"github.com/alauda/tracer/pkg/utils"
	"github.com/robfig/cron/v3"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/klog"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"time"
)

// TraceJobReconciler reconciles a TraceJob object
type TraceJobReconciler struct {
	client.Client
	Role   string
	Scheme *runtime.Scheme
	Clock
}

type realClock struct{}

func (_ realClock) Now() time.Time { return time.Now() }

type Clock interface {
	Now() time.Time
}

// +kubebuilder:rbac:groups=tracer.alauda.io,resources=tracejobs,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=tracer.alauda.io,resources=tracejobs/status,verbs=get;update;patch

func (r *TraceJobReconciler) Reconcile(req ctrl.Request) (result ctrl.Result, err error) {
	ctx := context.Background()
	start := time.Now()
	klog.Infof("start reconcile %s", req.Name)
	defer klog.Infof("finish reconcile %s, spend time %s, err %+v", req.Name, time.Since(start).String(), err)

	traceJob := &tracerv1beta1.TraceJob{}
	err = r.Get(ctx, req.NamespacedName, traceJob)
	if err != nil {
		klog.Error("unable to fetch trace job", err)
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}
	if traceJob.Spec.Schedule == "" {
		if !labels.SelectorFromSet(utils.ForceExecuteLabel).Matches(labels.Set(traceJob.Labels)) {
			return ctrl.Result{}, nil
		}
		if err := r.createTraceJobReport(ctx, traceJob); err != nil {
			klog.Error("create tracejob report failed", err)
			return ctrl.Result{}, err
		}
		traceJob.Status.LastScheduleTime = &metav1.Time{Time: time.Now()}
		if err := r.Status().Update(ctx, traceJob); err != nil {
			return ctrl.Result{}, err
		}
		if err := r.removeForceExecuteLabel(ctx, traceJob); err != nil {
			return ctrl.Result{}, err
		}
		return ctrl.Result{}, nil
	}
	missedRun, nextRun, err := getNextSchedule(traceJob, r.Now())
	if err != nil {
		klog.Error("unable to figure trace job schedule", err)
		return ctrl.Result{}, nil
	}
	scheduledResult := ctrl.Result{RequeueAfter: nextRun.Sub(r.Now())}
	klog.Infof("now: %s, next run: %s", r.Now(), nextRun)
	if missedRun == nil && !labels.SelectorFromSet(utils.ForceExecuteLabel).Matches(labels.Set(traceJob.Labels)) {
		klog.Info("no upcoming scheduled times, sleeping until next")
		return scheduledResult, nil
	}
	if err := r.createTraceJobReport(ctx, traceJob); err != nil {
		klog.Error("create tracejob report failed", err)
		return ctrl.Result{}, err
	}
	traceJob.Status.LastScheduleTime = &metav1.Time{Time: time.Now()}
	if err := r.Status().Update(ctx, traceJob); err != nil {
		return ctrl.Result{}, err
	}
	if err := r.removeForceExecuteLabel(ctx, traceJob); err != nil {
		return ctrl.Result{}, err
	}
	return ctrl.Result{}, nil
}

func isJobFinished(report *tracerv1beta1.TraceJobReport) bool {
	return report.Status.Phase != tracerv1beta1.Running
}

func (r *TraceJobReconciler) removeForceExecuteLabel(ctx context.Context, traceJob *tracerv1beta1.TraceJob) error {
	if traceJob.Labels[utils.ForceExecuteKey] != "" {
		originTraceJob := traceJob.DeepCopy()
		delete(traceJob.Labels, utils.ForceExecuteKey)
		if err := r.Patch(ctx, traceJob, client.MergeFrom(originTraceJob)); err != nil {
			return err
		}
	}
	return nil
}

func (r *TraceJobReconciler) createTraceJobReport(ctx context.Context, traceJob *tracerv1beta1.TraceJob) error {
	traceJob.Status.LastScheduleTime = &metav1.Time{Time: time.Now()}
	traceJob.Status.LastExecuteReason = ""
	traceJob.Status.LastExecuteResult = tracerv1beta1.Running
	if err := r.Status().Update(ctx, traceJob); err != nil {
		klog.Error("update tracejob last schedule time failed", err)
		return err
	}
	reportName := fmt.Sprintf("%s-%d", traceJob.Name, utils.GetReverseTimestamp())
	report := &tracerv1beta1.TraceJobReport{
		ObjectMeta: metav1.ObjectMeta{
			Name: reportName,
			Labels: map[string]string{
				commonconf.JobLabel: traceJob.Name,
			},
		},
		Spec: tracerv1beta1.TraceJobReportSpec{AllAgents: []tracerv1beta1.AgentInfo{}},
	}
	controllerutil.SetControllerReference(traceJob, report, r.Scheme)
	var qualifiedPods []corev1.Pod
	pods := &corev1.PodList{}
	if err := r.List(ctx, pods, client.InNamespace(commonconf.PodNamespace), client.MatchingLabels(utils.AgentLabel)); err != nil {
		klog.Error("list agent pods failed", err)
		return err
	}
	for _, pod := range pods.Items {
		if pod.Status.PodIP == "" {
			// TODO: support dual stack
			continue
		}
		if pod.Status.Phase != corev1.PodRunning {
			continue
		}
		qualifiedPods = append(qualifiedPods, pod)
	}
	for _, pod := range qualifiedPods {
		report.Spec.AllAgents = append(report.Spec.AllAgents, tracerv1beta1.AgentInfo{
			PodName:      pod.Name,
			PodNamespace: pod.Namespace,
			PodIP:        pod.Status.PodIP,
			NodeName:     pod.Spec.NodeName,
			NetworkMode:  getPodNetworkMode(pod),
		})
	}
	report.Spec.Probes = traceJob.Spec.Probes
	for _, bp := range traceJob.Spec.BuiltInProbes {
		klog.Infof("tracejob: %s load builtInProbes: %s", traceJob.Name, bp)
		report.Spec.Probes = append(report.Spec.Probes, probe.LoadBuiltInProbes(bp, report.Spec.AllAgents)...)
	}
	if err := r.Create(ctx, report); err != nil {
		klog.Error("create report for tracejob failed", err)
		return err
	}
	klog.Info("create traceJobReport: ", reportName)
	report.Status = tracerv1beta1.TraceJobReportStatus{
		Phase:          tracerv1beta1.Running,
		Detail:         []tracerv1beta1.Probe{},
		ReportedAgents: []tracerv1beta1.AgentInfo{},
		StartTime:      metav1.Time{Time: time.Now()},
	}
	if err := r.Status().Update(ctx, report); err != nil {
		klog.Error("update report status failed", err)
		return err
	}
	return nil
}

func getNextSchedule(traceJob *tracerv1beta1.TraceJob, now time.Time) (lastMissed *time.Time, next time.Time, err error) {
	sched, err := cron.ParseStandard(traceJob.Spec.Schedule)
	if err != nil {
		return nil, time.Time{}, fmt.Errorf("Unparseable schedule %q: %v", traceJob.Spec.Schedule, err)
	}
	var earliestTime time.Time
	if traceJob.Status.LastScheduleTime != nil {
		earliestTime = traceJob.Status.LastScheduleTime.Time
	} else {
		earliestTime = traceJob.ObjectMeta.CreationTimestamp.Time
	}
	if earliestTime.After(now) {
		return nil, sched.Next(now), nil
	}

	starts := 0
	for t := sched.Next(earliestTime); !t.After(now); t = sched.Next(t) {
		lastMissed = &t
		starts++
		if starts > 100 {
			return nil, time.Time{}, fmt.Errorf("Too many missed start times")
		}
	}
	return lastMissed, sched.Next(now), nil
}

func getPodNetworkMode(pod corev1.Pod) string {
	if pod.Spec.HostNetwork {
		return tracerv1beta1.HostNetwork
	}
	return tracerv1beta1.ContainerNetwork
}

func (r *TraceJobReconciler) SetupWithManager(mgr ctrl.Manager) error {
	if r.Role != utils.RoleManager {
		return nil
	}

	if r.Clock == nil {
		r.Clock = realClock{}
	}

	return ctrl.NewControllerManagedBy(mgr).
		For(&tracerv1beta1.TraceJob{}).
		Owns(&tracerv1beta1.TraceJobReport{}).
		Named("tracejob_controller").
		Complete(r)
}
