/*

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"encoding/json"
	"errors"
	agentconf "github.com/alauda/tracer/pkg/config/agent"
	commonconf "github.com/alauda/tracer/pkg/config/common"
	managerconf "github.com/alauda/tracer/pkg/config/manager"
	"github.com/alauda/tracer/pkg/metrics"
	"github.com/alauda/tracer/pkg/probe"
	"github.com/alauda/tracer/pkg/utils"
	"github.com/spf13/cast"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/klog"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sort"
	"sync"
	"time"

	tracerv1beta1 "github.com/alauda/tracer/api/v1beta1"
)

// TraceJobReportReconciler reconciles a TraceJobReport object
type TraceJobReportReconciler struct {
	client.Client
	Role   string
	Scheme *runtime.Scheme
}

// +kubebuilder:rbac:groups=tracer.alauda.io,resources=tracejobreports,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=tracer.alauda.io,resources=tracejobreports/status,verbs=get;update;patch

func (r *TraceJobReportReconciler) Reconcile(req ctrl.Request) (result ctrl.Result, err error) {
	ctx := context.Background()
	start := time.Now()
	klog.Infof("start reconcile %s", req.Name)
	defer klog.Infof("finish reconcile %s, spend time %s, err %+v", req.Name, time.Since(start).String(), err)

	traceJobReport := &tracerv1beta1.TraceJobReport{}
	err = r.Get(ctx, req.NamespacedName, traceJobReport)
	if err != nil {
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}
	if r.Role == utils.RoleManager {
		if traceJobReport.Status.StartTime != (metav1.Time{}) && traceJobReport.Status.ObservedGeneration != traceJobReport.ObjectMeta.Generation {
			bs := utils.GenObservedGenerationPatch(traceJobReport.ObjectMeta.Generation)
			if err := r.Status().Patch(ctx, traceJobReport, client.RawPatch(types.JSONPatchType, bs)); err != nil {
				klog.Error("failed to patch observed generation", err)
				return ctrl.Result{}, err
			}
		}
		traceJob, err := r.getTraceJobByReport(ctx, traceJobReport)
		if err != nil {
			if k8serrors.IsNotFound(err) {
				if e := r.Delete(ctx, traceJobReport); e != nil {
					klog.Error("delete tracejob report failed", e)
					return ctrl.Result{}, e
				}
				return ctrl.Result{}, nil
			}
			return ctrl.Result{}, err
		}
		needRequeue := false
		if !isJobFinished(traceJobReport) {
			if err := r.resolveReportStatus(ctx, traceJobReport); err != nil {
				return ctrl.Result{}, client.IgnoreNotFound(err)
			}
			if err := r.reportToJob(ctx, traceJob, traceJobReport); err != nil {
				return ctrl.Result{}, client.IgnoreNotFound(err)
			}
			if !isJobFinished(traceJobReport) {
				needRequeue = true
			}
		}
		if err := r.cleanStaleReport(ctx, traceJob, traceJobReport); err != nil {
			return ctrl.Result{}, err
		}
		if needRequeue {
			return ctrl.Result{RequeueAfter: 5 * time.Second}, nil
		}
	} else if r.Role == utils.RoleAgent {
		if traceJobReport.Status.ObservedGeneration == traceJobReport.ObjectMeta.Generation {
			// status changed, skip
			return ctrl.Result{}, nil
		}
		if isJobFinished(traceJobReport) {
			return ctrl.Result{}, nil
		}
		needProbeAgent := false
		for _, a := range traceJobReport.Spec.AllAgents {
			if a.PodName == commonconf.PodName {
				reported := false
				for _, b := range traceJobReport.Status.ReportedAgents {
					if b.PodName == commonconf.PodName {
						reported = true
						break
					}
				}
				if !reported {
					needProbeAgent = true
				}
				break
			}
		}
		if !needProbeAgent {
			return ctrl.Result{}, nil
		}
		r.Do(traceJobReport)
	}
	return ctrl.Result{}, nil
}

func (r *TraceJobReportReconciler) Do(report *tracerv1beta1.TraceJobReport) {
	wg := sync.WaitGroup{}
	jobsChan := make(chan struct{}, agentconf.Concurrency)
	resultChan := make(chan tracerv1beta1.Probe, len(report.Spec.Probes))
	now := time.Now()
	for _, p := range report.Spec.Probes {
		wg.Add(1)
		jobsChan <- struct{}{}
		go func(p tracerv1beta1.Probe) {
			klog.Infof("doing %s %s %d %s %s", p.Protocol, p.Destination.IP, cast.ToInt32(p.Destination.Port), p.Destination.URL, p.Destination.Domain)
			err := probe.Check(p)
			klog.Infof("finish %s %s %d %s %s", p.Protocol, p.Destination.IP, cast.ToInt32(p.Destination.Port), p.Destination.URL, p.Destination.Domain)
			p.Source = &tracerv1beta1.AddressInfo{
				IP:           commonconf.PodIP,
				PodName:      commonconf.PodName,
				PodNamespace: commonconf.PodNamespace,
				NodeName:     commonconf.NodeName,
				NetworkMode:  commonconf.NetworkMode,
			}
			if err != nil {
				klog.V(4).Infof("probe %+v failed, err: %+v", p, err)
				p.Result = false
				p.Reason = err.Error()
				resultChan <- p
			} else {
				klog.V(4).Infof("probe %+v success", p)
				p.Result = true
				resultChan <- p
			}
			<-jobsChan
			wg.Done()
		}(p)
	}
	klog.Info("waiting for task done")
	wg.Wait()
	klog.Infof("task execution takes: %.2fs", time.Since(now).Seconds())
	close(resultChan)
	var helper utils.ResultTmplHelper
	for r := range resultChan {
		b, err := json.Marshal(r)
		if err != nil {
			klog.Errorf("marshal json failed: %+v", err)
			return
		}
		helper.ProbeResults = append(helper.ProbeResults, string(b))
	}
	agentInfo := tracerv1beta1.AgentInfo{
		PodName:      commonconf.PodName,
		PodNamespace: commonconf.PodNamespace,
		PodIP:        commonconf.PodIP,
		NodeName:     commonconf.NodeName,
		NetworkMode:  commonconf.NetworkMode,
	}
	b, err := json.Marshal(agentInfo)
	if err != nil {
		klog.Errorf("marshal json failed: %+v", err)
		return
	}
	helper.AgentInfo = string(b)
	patches, err := utils.GenResultPatch(helper)
	if err != nil {
		klog.Errorf("generate patch failed: %+v", err)
		return
	}
	if err := r.Status().Patch(context.TODO(), report, client.RawPatch(types.JSONPatchType, patches)); err != nil {
		klog.Errorf("patch report %s failed, %v", report.Name, err)
		return
	}
}

func isAllAgentReport(reportedAgents, allAgents []tracerv1beta1.AgentInfo) bool {
	for _, aa := range allAgents {
		found := false
		for _, ra := range reportedAgents {
			if aa.PodName == ra.PodName {
				found = true
				break
			}
		}
		if !found {
			return false
		}
	}
	return true
}

func (r *TraceJobReportReconciler) resolveReportStatus(ctx context.Context, traceJobReport *tracerv1beta1.TraceJobReport) error {
	prePhase := traceJobReport.Status.Phase
	defer func() {
		klog.Infof("finish resolve %s status %s", traceJobReport.Name, traceJobReport.Status.Phase)
		if traceJobReport.Status.Phase != prePhase {
			metrics.UpdateMetrics(traceJobReport)
		}
	}()
	if !isJobFinished(traceJobReport) {
		originReport := traceJobReport.DeepCopy()
		if isAllAgentReport(traceJobReport.Status.ReportedAgents, traceJobReport.Spec.AllAgents) {
			success := true
			for _, r := range traceJobReport.Status.Detail {
				if r.Result == false {
					success = false
					break
				}
			}
			if success {
				traceJobReport.Status.Phase = tracerv1beta1.Success
				traceJobReport.Status.Reason = ""
				traceJobReport.Status.FinishTime = &metav1.Time{Time: time.Now()}
			} else {
				traceJobReport.Status.Phase = tracerv1beta1.Failed
				traceJobReport.Status.Reason = "some probes failed"
				traceJobReport.Status.FinishTime = &metav1.Time{Time: time.Now()}
			}
			if err := r.Client.Status().Patch(ctx, traceJobReport, client.MergeFrom(originReport)); err != nil {
				return err
			}
			return nil
		}
		if traceJobReport.Status.StartTime.Add(managerconf.JobTimeout).Before(time.Now()) {
			traceJobReport.Status.Phase = tracerv1beta1.Failed
			traceJobReport.Status.Reason = "execution timeout"
			if err := r.Client.Status().Patch(ctx, traceJobReport, client.MergeFrom(originReport)); err != nil {
				return err
			}
		}
		return nil
	}
	return nil
}

func (r *TraceJobReportReconciler) getTraceJobByReport(ctx context.Context, traceJobReport *tracerv1beta1.TraceJobReport) (*tracerv1beta1.TraceJob, error) {
	traceJob := &tracerv1beta1.TraceJob{}
	if len(traceJobReport.OwnerReferences) != 1 {
		return nil, errors.New("invalid ownerreference")
	}
	if err := r.Get(ctx, types.NamespacedName{Name: traceJobReport.OwnerReferences[0].Name}, traceJob); err != nil {
		return nil, err
	}
	return traceJob, nil
}

func (r *TraceJobReportReconciler) cleanStaleReport(ctx context.Context, traceJob *tracerv1beta1.TraceJob, traceJobReport *tracerv1beta1.TraceJobReport) error {
	if traceJob.Spec.FailedJobsHistoryLimit == nil && traceJob.Spec.SuccessfulJobsHistoryLimit == nil {
		return nil
	}
	var (
		failedLimit  int32 = 1e9
		successLimit int32 = 1e9
	)
	if traceJob.Spec.FailedJobsHistoryLimit != nil {
		failedLimit = *traceJob.Spec.FailedJobsHistoryLimit
	}
	if traceJob.Spec.SuccessfulJobsHistoryLimit != nil {
		successLimit = *traceJob.Spec.SuccessfulJobsHistoryLimit
	}
	reportList := &tracerv1beta1.TraceJobReportList{}
	if err := r.List(ctx, reportList, client.MatchingLabels{commonconf.JobLabel: traceJob.Name}); err != nil {
		return client.IgnoreNotFound(err)
	}
	sort.Sort(reportList)
	var (
		successCnt int32
		failCnt    int32
	)
	for _, rp := range reportList.Items {
		if rp.Status.Phase == tracerv1beta1.Success {
			successCnt++
			if successCnt > successLimit {
				klog.Infof("delete old success report %s", rp.Name)
				if err := r.Delete(ctx, &rp); err != nil {
					klog.Error("delete exceed report failed", err)
				}
			}
		} else if rp.Status.Phase == tracerv1beta1.Failed {
			failCnt++
			if failCnt > failedLimit {
				klog.Infof("delete old failed job report %s", rp.Name)
				if err := r.Delete(ctx, &rp); err != nil {
					klog.Error("delete exceed report failed", err)
				}
			}
		}
	}
	return nil
}

func (r *TraceJobReportReconciler) reportToJob(ctx context.Context, traceJob *tracerv1beta1.TraceJob, traceJobReport *tracerv1beta1.TraceJobReport) error {
	originJob := traceJob.DeepCopy()
	if traceJobReport.Status.Phase != tracerv1beta1.Running {
		traceJob.Status.LastExecuteResult = traceJobReport.Status.Phase
		traceJob.Status.LastExecuteReason = traceJobReport.Status.Reason
	}
	if err := r.Status().Patch(ctx, traceJob, client.MergeFrom(originJob)); err != nil {
		return err
	}
	return nil
}

func (r *TraceJobReportReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&tracerv1beta1.TraceJobReport{}).
		Named("tracejobreport_controller").
		Complete(r)
}
