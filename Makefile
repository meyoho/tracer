STUB_VERSION=v0.0.6
# Image URL to use all building/pushing image targets
IMG ?= harbor-b.alauda.cn/tracer/tracer:latest
STUB_IMG ?= harbor-b.alauda.cn/tracer/tracer:${STUB_VERSION}
# Produce CRDs that work back to Kubernetes 1.11 (no version conversion)
CRD_OPTIONS ?= "crd:trivialVersions=true"
GIT_COMMIT=$(shell git rev-parse HEAD)
BUILD_DATE=$(shell date -u '+%Y-%m-%d_%I:%M:%S%p')
VERSION_PACKAGE=github.com/alauda/tracer/pkg/version

# Get the currently used golang install path (in GOPATH/bin, unless GOBIN is set)
ifeq (,$(shell go env GOBIN))
GOBIN=$(shell go env GOPATH)/bin
else
GOBIN=$(shell go env GOBIN)
endif

all: preflight manager plugin

# Run tests
test: generate fmt vet manifests
	KUBEBUILDER_ATTACH_CONTROL_PLANE_OUTPUT=true go test ./... -coverprofile cover.out

preflight: generate fmt vet

# Build manager binary
manager:
	CGO_ENABLED=0 go build -v -ldflags '-X "${VERSION_PACKAGE}.GitCommit=${GIT_COMMIT}" -X "${VERSION_PACKAGE}.BuildDate=${BUILD_DATE}"' -o bin/manager main.go

# Build kubectl plugin binary
plugin:
	CGO_ENABLED=0 go build -v -ldflags '-X "${VERSION_PACKAGE}.GitCommit=${GIT_COMMIT}" -X "${VERSION_PACKAGE}.BuildDate=${BUILD_DATE}"' -o bin/kubectl-tracer cmd/plugin/main.go

# Run against the configured Kubernetes cluster in ~/.kube/config
run-manager: generate fmt vet manifests
	go run cmd/manager/main.go

run-agent: generate fmt vet manifests
	go run cmd/agent/main.go

# Install CRDs into a cluster
install: manifests
	kustomize build config/crd | kubectl apply -f -

# Uninstall CRDs from a cluster
uninstall: manifests
	kustomize build config/crd | kubectl delete -f -

# Deploy controller in the configured Kubernetes cluster in ~/.kube/config
deploy: manifests
	cd config/manager && kustomize edit set image controller=${IMG}
	kustomize build config/default | kubectl apply -f -

# Generate manifests e.g. CRD, RBAC etc.
manifests: controller-gen
	$(CONTROLLER_GEN) $(CRD_OPTIONS) rbac:roleName=manager-role webhook paths="./..." output:crd:artifacts:config=config/crd/bases
	cat config/crd/bases/*.yaml > deploy/crd.yaml

# Run go fmt against code
fmt:
	go fmt ./...

# Run go vet against code
vet:
	go vet ./...

# Generate code
generate: controller-gen
	$(CONTROLLER_GEN) object:headerFile=./hack/boilerplate.go.txt paths="./..."

# Build the docker image
docker-build:
	docker build . -t ${IMG} -f build/Dockerfile

docker-build-stub:
	docker build ./build -t ${STUB_IMG} -f build/Dockerfile.stub

# Push the docker image
docker-push:
	docker push ${IMG}

# find or download controller-gen
# download controller-gen if necessary
controller-gen:
ifeq (, $(shell which controller-gen))
	@{ \
	set -e ;\
	CONTROLLER_GEN_TMP_DIR=$$(mktemp -d) ;\
	cd $$CONTROLLER_GEN_TMP_DIR ;\
	go mod init tmp ;\
	go get sigs.k8s.io/controller-tools/cmd/controller-gen@v0.2.4 ;\
	rm -rf $$CONTROLLER_GEN_TMP_DIR ;\
	}
CONTROLLER_GEN=$(GOBIN)/controller-gen
else
CONTROLLER_GEN=$(shell which controller-gen)
endif
