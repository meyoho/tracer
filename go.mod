module github.com/alauda/tracer

go 1.13

require (
	github.com/briandowns/spinner v1.9.0
	github.com/miekg/dns v1.1.27
	github.com/olekukonko/tablewriter v0.0.4
	github.com/onsi/ginkgo v1.11.0
	github.com/onsi/gomega v1.8.1
	github.com/prometheus/client_golang v1.0.0
	github.com/robfig/cron/v3 v3.0.1
	github.com/sparrc/go-ping v0.0.0-20190613174326-4e5b6552494c
	github.com/spf13/cast v1.3.0
	github.com/spf13/cobra v0.0.5
	github.com/spf13/pflag v1.0.5
	github.com/spf13/viper v1.3.2
	golang.org/x/sys v0.0.0-20200116001909-b77594299b42 // indirect
	k8s.io/api v0.17.3
	k8s.io/apimachinery v0.17.3
	k8s.io/cli-runtime v0.17.3
	k8s.io/client-go v0.17.3
	k8s.io/klog v1.0.0
	k8s.io/utils v0.0.0-20191114184206-e782cd3c129f
	sigs.k8s.io/controller-runtime v0.5.2
)

replace github.com/sparrc/go-ping v0.0.0-20190613174326-4e5b6552494c => github.com/asmpro/go-ping v0.0.0-20200117090035-8f5c4312cd54
