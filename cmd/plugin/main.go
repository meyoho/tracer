package main

import (
	"os"

	"github.com/spf13/pflag"
	"k8s.io/klog"

	_ "github.com/alauda/tracer/pkg/config"
	"github.com/alauda/tracer/pkg/plugin"
	"k8s.io/cli-runtime/pkg/genericclioptions"
)

func main() {
	klog.InitFlags(nil)
	flags := pflag.NewFlagSet("kubectl-tracer", pflag.ExitOnError)
	pflag.CommandLine = flags

	root := plugin.NewTracerCmd(genericclioptions.IOStreams{In: os.Stdin, Out: os.Stdout, ErrOut: os.Stderr})
	if err := root.Execute(); err != nil {
		os.Exit(1)
	}
}
