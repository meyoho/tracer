package version

import "k8s.io/klog"

var (
	GitCommit string
	BuildDate string
)

func PrintVersion() {
	klog.Infof("GitCommit: %s", GitCommit)
	klog.Infof("BuildDate: %s", BuildDate)
}
