package plugin

import (
	alaudaiov1beta1 "github.com/alauda/tracer/api/v1beta1"
	"github.com/spf13/cobra"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/cli-runtime/pkg/genericclioptions"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	"k8s.io/klog"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

var (
	scheme        = runtime.NewScheme()
	ignoreSuccess bool
)

func init() {
	_ = clientgoscheme.AddToScheme(scheme)
	_ = alaudaiov1beta1.AddToScheme(scheme)
}

var pctx *TracerContext = nil

type TracerContext struct {
	Client client.Client
	flags  *genericclioptions.ConfigFlags
}

func NewTracerContext() *TracerContext {
	return &TracerContext{
		flags: genericclioptions.NewConfigFlags(true),
	}
}

func (t *TracerContext) Complete() error {
	configLoader := t.flags.ToRawKubeConfigLoader()

	cfg, err := configLoader.ClientConfig()
	if err != nil {
		klog.Errorf("initial rest.Config obj config failed, err: %v", err)
		return err
	}

	t.Client, err = client.New(cfg, client.Options{Scheme: scheme})
	if err != nil {
		klog.Errorf("init k8s client failed, err: %v", err)
		return err
	}

	return nil
}

func NewTracerCmd(streams genericclioptions.IOStreams) *cobra.Command {
	pctx = NewTracerContext()
	cmd := &cobra.Command{
		Use:   "tracer",
		Short: "diagnonse cluster network",
		PersistentPreRunE: func(cmd *cobra.Command, args []string) error {
			err := pctx.Complete()
			if err != nil {
				return err
			}
			return nil
		},
	}
	cmd.AddCommand(NewDiagnoseCommand())
	cmd.AddCommand(NewInspectCommand())
	cmd.AddCommand(NewVersionCommand())
	return cmd
}
