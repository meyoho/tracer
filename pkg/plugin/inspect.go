package plugin

import (
	"context"
	"fmt"
	tracerv1beta1 "github.com/alauda/tracer/api/v1beta1"
	"github.com/olekukonko/tablewriter"
	"github.com/spf13/cobra"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/klog"
	"os"
	"reflect"
	"sort"
	"strconv"
)

var (
	traceReportName string
)

func inspect(rName string) error {
	ctx := context.Background()

	traceJobReport := &tracerv1beta1.TraceJobReport{}
	if err := pctx.Client.Get(ctx, types.NamespacedName{Name: rName}, traceJobReport); err != nil {
		return err
	}
	klog.Infof("traceJob: %+v", traceJobReport.Labels)
	klog.Info("Phase:", traceJobReport.Status.Phase)
	if traceJobReport.Status.Reason != "" {
		klog.Info("Reason:", traceJobReport.Status.Reason)
	}
	klog.Info("startTime:", traceJobReport.Status.StartTime)
	if traceJobReport.Status.FinishTime != nil {
		klog.Info("finishTime:", traceJobReport.Status.FinishTime)
	}
	unreportData := [][]string{}
	finishedData := [][]string{}
	for _, agent := range traceJobReport.Spec.AllAgents {
		found := false
		for _, ragent := range traceJobReport.Status.ReportedAgents {
			if reflect.DeepEqual(agent, ragent) {
				found = true
				break
			}
		}
		if !found {
			unreportData = append(unreportData, []string{agent.NodeName,
				fmt.Sprintf("%s/%s", agent.PodNamespace, agent.PodName),
				agent.NetworkMode})
		}
	}
	for _, probe := range traceJobReport.Status.Detail {
		if ignoreSuccess && probe.Result {
			continue
		}
		finishedData = append(finishedData, []string{
			probe.Source.NodeName,
			fmt.Sprintf("%s/%s", probe.Source.PodNamespace, probe.Source.PodName),
			probe.Source.NetworkMode,
			probe.Protocol,
			probe.Destination.String(),
			strconv.FormatBool(probe.Result),
			probe.Reason,
		})
	}
	resultIdx := 5
	podIdx := 1
	sort.Slice(finishedData, func(i, j int) bool {
		if finishedData[i][resultIdx] != finishedData[j][resultIdx] {
			return finishedData[i][resultIdx] < finishedData[j][resultIdx]
		}
		return finishedData[i][podIdx] < finishedData[j][podIdx]
	})

	klog.Info("\nagent report result")
	finishedTable := tablewriter.NewWriter(os.Stdout)
	finishedTable.SetHeader([]string{"Node", "Pod", "NetworkMode", "Protocol", "Destination", "Result", "Reason"})
	finishedTable.AppendBulk(finishedData)
	finishedTable.Render() // Send output

	if len(unreportData) > 0 {
		klog.Error("\nSome agent not report result")
		unfinishedTable := tablewriter.NewWriter(os.Stderr)
		unfinishedTable.SetHeader([]string{"Node", "Pod", "NetworkMode"})
		unfinishedTable.SetColumnColor(tablewriter.Colors{tablewriter.Bold, tablewriter.FgHiRedColor},
			tablewriter.Colors{tablewriter.Bold, tablewriter.FgHiRedColor},
			tablewriter.Colors{tablewriter.Bold, tablewriter.FgHiRedColor})
		unfinishedTable.AppendBulk(unreportData)
		unfinishedTable.Render()
	}

	return nil
}

func NewInspectCommand() *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "inspect",
		Short: "show traceJobReport detail",
		RunE: func(cmd *cobra.Command, args []string) error {
			return inspect(traceReportName)
		},
	}
	cmd.Flags().StringVarP(&traceReportName, "tracejobreport", "r", "", "the tracejobreport you want to inspect")
	cmd.Flags().BoolVarP(&ignoreSuccess, "ignore", "i", false, "ignore success result")
	cobra.MarkFlagRequired(cmd.Flags(), "tracejobreport")
	return cmd
}
