package plugin

import (
	"context"
	"crypto/md5"
	"encoding/hex"
	"errors"
	tracerv1beta1 "github.com/alauda/tracer/api/v1beta1"
	"github.com/alauda/tracer/pkg/probe"
	"github.com/alauda/tracer/pkg/utils"
	"github.com/briandowns/spinner"
	"github.com/spf13/cobra"
	"k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/klog"
	"k8s.io/utils/pointer"
	"time"
)

var (
	keepJob         bool
	diagnoseTimeout time.Duration
)

func diagnose() error {
	ctx := context.Background()

	h := md5.New()
	h.Write([]byte(time.Now().String()))
	tjname := hex.EncodeToString(h.Sum(nil))
	traceJob := &tracerv1beta1.TraceJob{
		ObjectMeta: v1.ObjectMeta{
			Name:   tjname,
			Labels: utils.ForceExecuteLabel,
		},
		Spec: tracerv1beta1.TraceJobSpec{
			BuiltInProbes:              []string{string(probe.PodPod), string(probe.PodHost), string(probe.InternalDNS)},
			FailedJobsHistoryLimit:     pointer.Int32Ptr(20),
			SuccessfulJobsHistoryLimit: pointer.Int32Ptr(20),
		},
	}
	if err := pctx.Client.Create(ctx, traceJob); err != nil {
		return err
	}
	if !keepJob {
		defer pctx.Client.Delete(ctx, traceJob)
	}
	klog.Infof("setup tracejob: %s, waiting", tjname)
	s := spinner.New(spinner.CharSets[9], 100*time.Millisecond)
	s.FinalMSG = "Complete!\n"
	s.Start()
	startTime := time.Now()
	var tjReport *tracerv1beta1.TraceJobReport
	for {
		// controller-runtime dont provide a convenient watch method, use get instead.
		if tjReport == nil {
			reportList := &tracerv1beta1.TraceJobReportList{}
			if err := pctx.Client.List(ctx, reportList); err != nil {
				return err
			}
			for _, report := range reportList.Items {
				for _, o := range report.GetOwnerReferences() {
					if o.Name == tjname && o.Kind == tracerv1beta1.TraceJobKind {
						tjReport = &report
						klog.Infof("get tracejobreport: %s, waiting", report.Name)
						goto OUT
					}
				}
			}
		} else {
			if err := pctx.Client.Get(ctx, types.NamespacedName{Name: tjReport.Name}, tjReport); err != nil {
				return err
			}
		}
	OUT:
		if tjReport != nil && tjReport.Status.Phase != tracerv1beta1.Running {
			s.Stop()
			return inspect(tjReport.Name)
		}
		if startTime.Add(diagnoseTimeout + 10*time.Second).Before(time.Now()) {
			return errors.New("execution timeout")
		}
		time.Sleep(time.Second)
	}
}

func NewDiagnoseCommand() *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "diagnose",
		Short: "do a network diagnose for current cluster",
		RunE: func(cmd *cobra.Command, args []string) error {
			if err := diagnose(); err != nil {
				return err
			}
			return nil
		},
	}
	cmd.Flags().BoolVarP(&keepJob, "keep", "k", false, "keep tracejob")
	cmd.Flags().BoolVarP(&ignoreSuccess, "ignore", "i", false, "ignore success result")
	cmd.Flags().DurationVarP(&diagnoseTimeout, "timeout", "t", time.Minute, "diagnose timeout")
	return cmd
}
