package plugin

import (
	"github.com/alauda/tracer/pkg/version"
	"github.com/spf13/cobra"
)

func NewVersionCommand() *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "version",
		Short: "Print the version number of kubectl-tracer",
		Run: func(cmd *cobra.Command, args []string) {
			version.PrintVersion()
		},
	}
	return cmd
}
