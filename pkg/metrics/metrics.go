package metrics

import (
	"fmt"
	tracerv1beta1 "github.com/alauda/tracer/api/v1beta1"
	"github.com/alauda/tracer/pkg/probe"
	"github.com/prometheus/client_golang/prometheus"
	"k8s.io/klog"
	"sigs.k8s.io/controller-runtime/pkg/metrics"
)

var (
	ProtocolErrCounters = map[string]*prometheus.CounterVec{}
	NoReportCounters    *prometheus.CounterVec
)

func init() {
	for _, protocol := range probe.ImplProtocols {
		ProtocolErrCounters[string(protocol)] = prometheus.NewCounterVec(
			prometheus.CounterOpts{
				Name: fmt.Sprintf("protocol_%s_errors_total", protocol),
				Help: fmt.Sprintf("Number of protocol %s probe failed", protocol),
			},
			[]string{"node", "networkMode"},
		)
		metrics.Registry.MustRegister(ProtocolErrCounters[string(protocol)])
	}
	NoReportCounters = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: fmt.Sprintf("agent_no_report_errors_total"),
			Help: fmt.Sprintf("Number of agent not report"),
		},
		[]string{"node", "networkMode"},
	)
	metrics.Registry.MustRegister(NoReportCounters)
}

func UpdateMetrics(report *tracerv1beta1.TraceJobReport) {
	for _, agent1 := range report.Spec.AllAgents {
		found := false
		for _, agent2 := range report.Status.ReportedAgents {
			if agent1.PodName == agent2.PodName && agent1.PodNamespace == agent2.PodNamespace {
				found = true
				break
			}
		}
		if !found {
			NoReportCounters.With(prometheus.Labels{"node": agent1.NodeName, "networkMode": agent1.NetworkMode}).Inc()
			klog.Warningf("pod: %s on %s not report result", agent1.PodName, agent1.NodeName)
		}
	}
	for _, p := range report.Status.Detail {
		if !p.Result {
			ProtocolErrCounters[p.Protocol].With(prometheus.Labels{"node": p.Source.NodeName, "networkMode": p.Source.NetworkMode}).Inc()
		}
	}
}
