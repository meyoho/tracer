package utils

import "time"

func GetReverseTimestamp() int64 {
	// 2128/6/11 16:53:20
	return 5000000000 - time.Now().Unix()
}
