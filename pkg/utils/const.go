package utils

const (
	RoleManager = "manager"
	RoleAgent   = "agent"
	Domain      = "domain"
	Expected    = "expected"
)

var (
	AgentLabel        = map[string]string{"k8s-app": "tracer-agent"}
	ForceExecuteLabel = map[string]string{"force-execute": "true"}
	ForceExecuteKey   = "force-execute"
)
