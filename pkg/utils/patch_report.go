package utils

import (
	"bytes"
	"fmt"
	"k8s.io/klog"
	"text/template"
)

const (
	jsonPatchResultTmpl = `
[
	{{- range .ProbeResults }}
	{
		"op" : "add" ,
		"path" : "/status/detail/-" ,
		"value" : {{.}}
	},
	{{- end }}
	{
		"op" : "add" ,
		"path" : "/status/reportedAgents/-" ,
		"value" : {{.AgentInfo}}
	}
]
`
	jsonPatchObservedGenerationTmpl = `
[
	{
		"op" : "replace" ,
		"path" : "/status/observedGeneration" ,
		"value" : %d
	}
]
`
)

type ResultTmplHelper struct {
	ProbeResults []string
	AgentInfo    string
}

func GenResultPatch(helper ResultTmplHelper) ([]byte, error) {
	t := template.Must(template.New("").Parse(jsonPatchResultTmpl))
	var patchResult bytes.Buffer
	if err := t.Execute(&patchResult, helper); err != nil {
		return nil, err
	}
	bytes := patchResult.Bytes()
	klog.V(3).Infof("patch: %s", string(bytes))
	return bytes, nil
}

func GenObservedGenerationPatch(generation int64) []byte {
	return []byte(fmt.Sprintf(jsonPatchObservedGenerationTmpl, generation))
}
