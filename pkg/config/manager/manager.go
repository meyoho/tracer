package manager

import (
	"github.com/spf13/viper"
	"k8s.io/klog"
	"time"
)

var (
	JobTimeout time.Duration
)

func init() {
	viper.AutomaticEnv()
	viper.SetDefault("JOB_TIMEOUT", 60*time.Second)

	bindVar()
}

func bindVar() {
	JobTimeout = viper.GetDuration("JOB_TIMEOUT")
}

func PrintVar() {
	klog.Infof("JOB_TIMEOUT: %s", JobTimeout.String())
}
