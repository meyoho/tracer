package config

import (
	"github.com/alauda/tracer/pkg/config/agent"
	"github.com/alauda/tracer/pkg/config/common"
	"github.com/alauda/tracer/pkg/config/manager"
	"github.com/alauda/tracer/pkg/utils"
)

func PrintVar(role string) {
	common.PrintVar()
	switch role {
	case utils.RoleManager:
		manager.PrintVar()
	case utils.RoleAgent:
		agent.PrintVar()
	}
}
