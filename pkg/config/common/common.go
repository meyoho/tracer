package common

import (
	"github.com/spf13/viper"
	"k8s.io/klog"
)

var (
	PodName      string
	PodNamespace string
	PodIP        string
	NetworkMode  string
	NodeName     string
	JobLabel     string
)

func init() {
	viper.AutomaticEnv()
	viper.SetDefault("JOB_LABEL", "tracer.cpaas.io/jobName")

	bindVar()
}

func bindVar() {
	PodName = viper.GetString("POD_NAME")
	PodNamespace = viper.GetString("POD_NAMESPACE")
	PodIP = viper.GetString("POD_IP")
	NodeName = viper.GetString("NODE_NAME")
	JobLabel = viper.GetString("JOB_LABEL")
	NetworkMode = viper.GetString("NETWORK_MODE")
}

func PrintVar() {
	klog.Infof("POD_NAME: %s", PodName)
	klog.Infof("POD_NAMESPACE: %s", PodNamespace)
	klog.Infof("POD_IP: %s", PodIP)
	klog.Infof("NODE_NAME: %s", NodeName)
	klog.Infof("NETWORK_MODE: %s", NetworkMode)
}
