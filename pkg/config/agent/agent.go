package agent

import (
	"github.com/spf13/viper"
	"k8s.io/klog"
	"time"
)

var (
	Concurrency int
	HTTPTimeout time.Duration
	TCPTimeout  time.Duration
	ICMPTimeout time.Duration
	ICMPCount   int
	DNSTimeout  time.Duration

	HostDNSPort       int
	HostTCPPort       int
	HostUDPPort       int
	HostHTTPPort      int
	ContainerDNSPort  int
	ContainerTCPPort  int
	ContainerUDPPort  int
	ContainerHTTPPort int
)

func init() {
	viper.AutomaticEnv()
	viper.SetDefault("CONCURRENCY", 10)

	viper.SetDefault("HTTP_TIMEOUT", 2*time.Second)
	viper.SetDefault("TCP_TIMEOUT", time.Second)
	viper.SetDefault("ICMP_TIMEOUT", 3*time.Second)
	viper.SetDefault("ICMP_COUNT", 3)
	// define a timeout less than 5s
	viper.SetDefault("DNS_TIMEOUT", 2*time.Second)

	viper.SetDefault("CONTAINER_DNS_PORT", 53)
	viper.SetDefault("CONTAINER_HTTP_PORT", 80)
	bindVar()
}

func bindVar() {
	Concurrency = viper.GetInt("CONCURRENCY")

	HTTPTimeout = viper.GetDuration("HTTP_TIMEOUT")
	TCPTimeout = viper.GetDuration("TCP_TIMEOUT")
	ICMPTimeout = viper.GetDuration("ICMP_TIMEOUT")
	ICMPCount = viper.GetInt("ICMP_COUNT")
	DNSTimeout = viper.GetDuration("DNS_TIMEOUT")

	HostDNSPort = viper.GetInt("HOST_DNS_PORT")
	HostHTTPPort = viper.GetInt("HOST_HTTP_PORT")
	ContainerDNSPort = viper.GetInt("CONTAINER_DNS_PORT")
	ContainerHTTPPort = viper.GetInt("CONTAINER_HTTP_PORT")

	HostTCPPort = HostHTTPPort
	HostUDPPort = HostDNSPort
	ContainerTCPPort = ContainerHTTPPort
	ContainerUDPPort = ContainerDNSPort
}

func PrintVar() {
	klog.Infof("CONCURRENCY: %d", Concurrency)

	klog.Infof("HTTP_TIMEOUT: %s", HTTPTimeout)
	klog.Infof("TCP_TIMEOUT: %s", TCPTimeout)
	klog.Infof("ICMP_TIMEOUT: %s", ICMPTimeout)
	klog.Infof("ICMP_COUNT: %d", ICMPCount)
	klog.Infof("DNS_TIMEOUT: %s", DNSTimeout)

	klog.Infof("HOST_DNS_PORT: %d", HostDNSPort)
	klog.Infof("HOST_HTTP_PORT: %d", HostHTTPPort)
}
