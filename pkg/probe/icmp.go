package probe

import (
	"errors"
	agentconf "github.com/alauda/tracer/pkg/config/agent"
	"github.com/sparrc/go-ping"
	"time"
)

var (
	ICMPCount        = agentconf.ICMPCount
	ICMPTimeout      = agentconf.ICMPTimeout
	pingerPrivileged = true
)

func CheckICMP(addr string) error {
	pinger, err := ping.NewPinger(addr)
	if err != nil {
		return err
	}
	pinger.SetPrivileged(pingerPrivileged)
	pinger.Timeout = ICMPTimeout
	pinger.Count = ICMPCount
	pinger.Interval = 100 * time.Millisecond
	//pinger.Debug = true
	pinger.Run()
	if pinger.Statistics().PacketLoss != 0 {
		return errors.New("ping failed")
	}
	return nil
}
