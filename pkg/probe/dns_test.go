package probe

import (
	"fmt"
	"github.com/miekg/dns"
	"net"
	"strconv"
	"testing"
	"time"
)

var records = map[string]string{
	"kubernetes.default.svc.cluster.local.": "10.96.0.10",
}

func parseQuery(m *dns.Msg) {
	for _, q := range m.Question {
		switch q.Qtype {
		case dns.TypeA:
			ip := records[q.Name]
			if ip != "" {
				rr, err := dns.NewRR(fmt.Sprintf("%s A %s", q.Name, ip))
				if err == nil {
					m.Answer = append(m.Answer, rr)
				}
			}
		}
	}
}

func handleDnsRequest(w dns.ResponseWriter, r *dns.Msg) {
	m := new(dns.Msg)
	m.SetReply(r)
	m.Compress = false

	switch r.Opcode {
	case dns.OpcodeQuery:
		parseQuery(m)
	}

	w.WriteMsg(m)
}

func runFakeDNS(ch <-chan struct{}, port int) {
	dns.HandleFunc("local.", handleDnsRequest)

	// start server
	server := &dns.Server{Addr: fmt.Sprintf("127.0.0.1:%d", port), Net: "udp"}
	go server.ListenAndServe()
	select {
	case <-ch:
		server.Shutdown()
	}
}

func getFreePortUDP() (int, error) {
	addr, err := net.ResolveUDPAddr("udp", "localhost:0")
	if err != nil {
		return 0, err
	}

	l, err := net.ListenUDP("udp", addr)
	if err != nil {
		return 0, err
	}
	defer l.Close()
	return l.LocalAddr().(*net.UDPAddr).Port, nil
}

func TestCheckDNS(t *testing.T) {
	p, err := getFreePortUDP()
	if err != nil {
		panic(err)
	}
	ch := make(chan struct{})
	go runFakeDNS(ch, p)
	time.Sleep(3 * time.Second)
	dnsConfig = &dns.ClientConfig{
		Servers: []string{"127.0.0.1"},
		Port:    strconv.Itoa(p),
	}
	type args struct {
		domain string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			"kubernetes.default.svc.cluster.local",
			args{
				domain: "kubernetes.default.svc.cluster.local",
			},
			false,
		},
		{
			"no_record",
			args{
				domain: "no_record",
			},
			true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := CheckDNS(tt.args.domain); (err != nil) != tt.wantErr {
				t.Errorf("CheckDNS() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
	ch <- struct{}{}
}
