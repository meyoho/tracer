package probe

import (
	"fmt"
	tracerv1beta1 "github.com/alauda/tracer/api/v1beta1"
	agentconf "github.com/alauda/tracer/pkg/config/agent"
	"github.com/spf13/cast"
)

func loadImplProbe(podIP string, hostNetwork bool) []tracerv1beta1.Probe {
	var probes []tracerv1beta1.Probe
	for _, proto := range ImplProtocols {
		switch proto {
		case ICMP:
			p := tracerv1beta1.Probe{
				Protocol: string(proto),
				Destination: tracerv1beta1.AddressInfo{
					IP: podIP,
				},
			}
			probes = append(probes, p)
		case TCP:
			p := tracerv1beta1.Probe{
				Protocol: string(proto),
				Destination: tracerv1beta1.AddressInfo{
					IP: podIP,
				},
			}
			var port int32
			if hostNetwork {
				port = cast.ToInt32(agentconf.HostTCPPort)
			} else {
				port = cast.ToInt32(agentconf.ContainerTCPPort)
			}
			p.Destination.Port = &port
			probes = append(probes, p)
		case HTTP:
			p := tracerv1beta1.Probe{
				Protocol: string(proto),
			}
			if hostNetwork {
				p.Destination.URL = fmt.Sprintf("http://%s:%d", podIP, agentconf.HostHTTPPort)
			} else {
				p.Destination.URL = fmt.Sprintf("http://%s:%d", podIP, agentconf.ContainerHTTPPort)
			}
			probes = append(probes, p)
		default:
		}
	}
	return probes
}

func LoadBuiltInProbes(kind string, agents []tracerv1beta1.AgentInfo) []tracerv1beta1.Probe {
	var probes []tracerv1beta1.Probe
	switch kind {
	case string(PodHost):
		for _, a := range agents {
			if a.NetworkMode == tracerv1beta1.HostNetwork {
				probes = append(probes, loadImplProbe(a.PodIP, true)...)
			}
		}
		return probes
	case string(PodPod):
		for _, a := range agents {
			if a.NetworkMode == tracerv1beta1.ContainerNetwork {
				probes = append(probes, loadImplProbe(a.PodIP, false)...)
			}
		}
		return probes
	case string(InternalDNS):
		probes = append(probes, tracerv1beta1.Probe{
			Protocol: string(DNS),
			Destination: tracerv1beta1.AddressInfo{
				Domain: "kubernetes.default.svc.cluster.local",
			},
		})
		return probes
	default:
		return nil
	}
}
