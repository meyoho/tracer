package probe

import (
	"errors"
	"fmt"
	agentconf "github.com/alauda/tracer/pkg/config/agent"
	"github.com/miekg/dns"
	"strings"
)

var dnsTimeout = agentconf.DNSTimeout
var dnsConfig *dns.ClientConfig

func init() {
	dnsConfig, _ = dns.ClientConfigFromFile("/etc/resolv.conf")
}

func CheckDNS(domain string) error {
	if dnsConfig == nil {
		return errors.New("invalid connfig")
	}
	if len(dnsConfig.Servers) == 0 {
		return errors.New("no nameserver found")
	}
	m := dns.Msg{}
	if !strings.HasSuffix(domain, ".") {
		domain += "."
	}
	m.SetQuestion(domain, dns.TypeA)
	c := dns.Client{}
	c.Timeout = dnsTimeout
	r, _, err := c.Exchange(&m, fmt.Sprintf("%s:%s", dnsConfig.Servers[0], dnsConfig.Port))
	if err != nil {
		return err
	}
	if len(r.Answer) == 0 {
		return errors.New("no answers")
	}
	return nil
}
