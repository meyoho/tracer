package probe

import (
	"errors"
	tracerv1beta1 "github.com/alauda/tracer/api/v1beta1"
	"github.com/spf13/cast"
)

type Protocol string
type BuiltInProbe string

var (
	ICMP Protocol = "ICMP"
	UDP  Protocol = "UDP"
	TCP  Protocol = "TCP"
	HTTP Protocol = "HTTP"
	DNS  Protocol = "DNS"

	PodPod      BuiltInProbe = "pod_pod"
	PodHost     BuiltInProbe = "pod_host"
	InternalDNS BuiltInProbe = "internal_dns"

	ImplProtocols = []Protocol{ICMP, TCP, HTTP, DNS}
	ErrNotImpl    = errors.New("not implement")
)

func Check(p tracerv1beta1.Probe) error {
	var err error
	switch Protocol(p.Protocol) {
	case ICMP:
		err = CheckICMP(p.Destination.IP)
	case TCP:
		err = CheckTCP(p.Destination.IP, cast.ToString(p.Destination.Port))
	case HTTP:
		err = CheckHTTP(p.Destination.URL)
	case DNS:
		err = CheckDNS(p.Destination.Domain)
	case UDP:
		err = ErrNotImpl
	default:
		err = ErrNotImpl
	}
	return err
}
