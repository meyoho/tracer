package probe

import (
	"net"
	"strings"
	"testing"
)

func TestCheckTCP(t *testing.T) {
	listener, err := net.Listen("tcp", ":0")
	if err != nil {
		panic(err)
	}
	defer listener.Close()
	parts := strings.Split(listener.Addr().String(), ":")
	port := parts[len(parts)-1]
	type args struct {
		host string
		port string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{"localhost", args{"127.0.0.1", port}, false},
		{"invalid", args{"fake address", "80"}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := CheckTCP(tt.args.host, tt.args.port); (err != nil) != tt.wantErr {
				t.Errorf("CheckTCP() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
