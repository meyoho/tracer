package probe

import (
	"runtime"
	"testing"
)

func TestCheckICMP(t *testing.T) {
	if runtime.GOOS == "darwin" {
		pingerPrivileged = false
	}
	type args struct {
		addr string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{"localhost", args{"127.0.0.1"}, false},
		{"invalid", args{"fake address"}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := CheckICMP(tt.args.addr); (err != nil) != tt.wantErr {
				t.Errorf("CheckICMP() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
