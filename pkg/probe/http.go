package probe

import (
	"crypto/tls"
	agentconf "github.com/alauda/tracer/pkg/config/agent"
	"net/http"
)

var httpTimeout = agentconf.HTTPTimeout
var httpClient = http.Client{Timeout: httpTimeout}

func init() {
	customTransport := http.DefaultTransport.(*http.Transport).Clone()
	customTransport.TLSClientConfig = &tls.Config{InsecureSkipVerify: true}
	httpClient.Transport = customTransport
}

func CheckHTTP(url string) error {
	_, err := httpClient.Get(url)
	return err
}
