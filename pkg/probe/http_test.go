package probe

import (
	"fmt"
	"net"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestCheckHTTP(t *testing.T) {
	ts := httptest.NewUnstartedServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.RequestURI == "/200" {
			w.WriteHeader(200)
			return
		}
		if r.RequestURI == "/500" {
			w.WriteHeader(500)
			return
		}
	}))
	l, err := net.Listen("tcp", ":0")
	if err != nil {
		panic(err)
	}
	parts := strings.Split(l.Addr().String(), ":")
	port := parts[len(parts)-1]
	ts.Listener = l
	ts.Start()
	defer ts.Close()
	type args struct {
		url string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{"not listen",
			args{"http://localhost:65535"},
			true,
		},
		{"200",
			args{fmt.Sprintf("http://localhost:%s/200", port)},
			false,
		},
		{"500",
			args{fmt.Sprintf("http://localhost:%s/500", port)},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := CheckHTTP(tt.args.url); (err != nil) != tt.wantErr {
				t.Errorf("CheckHTTP() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
