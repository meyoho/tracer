package probe

import (
	"errors"
	agentconf "github.com/alauda/tracer/pkg/config/agent"
	"net"
)

var TCPTimeout = agentconf.TCPTimeout

func CheckTCP(host, port string) error {
	conn, err := net.DialTimeout("tcp", net.JoinHostPort(host, port), TCPTimeout)
	if err != nil {
		return err
	}
	if conn != nil {
		defer conn.Close()
		return nil
	}
	return errors.New("tcp dial failed")
}
